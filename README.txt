CONTENTS OF THIS FILE
---------------------
   
 * Installation
 * Configuration


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-8
   for further information.


CONFIGURATION
-------------

Once you've activated the module a new block will be 
available in Structure -> Block Layout. Choose the 
region in which you'd like to place the LinkedIn profile
plugin and press the button 'Place block'. In the modal
dialogue choose LinkedIn Profile Block. You will be 
presented with the 'Configure block' screen where you 
can provide a title (optional) for the block and also 
the URL of the LinkedIn Profile you want to display in
your block. Choose whether the profile is a member
profile or a company profile and choose a display mode. 
Hit save – and you're done.
