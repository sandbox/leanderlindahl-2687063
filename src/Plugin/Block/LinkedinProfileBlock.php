<?php
/**
 * @file
 * Contains \Drupal\linkedin_profile\Plugin\Block\LinkedInProfileBlock.
 */

namespace Drupal\linkedin_profile\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Block\BlockPluginInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a configurable block with Linked Profile's plugin.
 *
 * @Block(
 *   id = "linkedin_profile_block",
 *   admin_label = @Translation("LinkedIn Profile"),
 *   category = @Translation("LinkedIn Profile"),
 * )
 */
class LinkedinProfileBlock extends BlockBase implements BlockPluginInterface {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $config = $this->getConfiguration();

    if (!empty($config['profile_type'])) {
      $profile_type = $config['profile_type'];
    }
    else {
      $profile_type = $this->t('not set');
    }
    if (!empty($config['profile_url'])) {
      $profile_url = $config['profile_url'];
    }
    else {
      $profile_url = $this->t('not set');
    }
    if (!empty($config['display_mode'])) {
      $display_mode = $config['display_mode'];
    }
    else {
      $display_mode = $this->t('not set');
    }
    if (!empty($config['profile_name'])) {
      $profile_name = $config['profile_name'];
    }
    else {
      $profile_name = '';
    }

    return array(
      '#theme' => 'block--linkedin-profile',
      '#description' => t('description from block.php'),
      '#profile_type' => $profile_type,
      '#profile_url' => $profile_url,
      '#display_mode' => $display_mode,
      '#profile_name' => $profile_name,
    );
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);

    $config = $this->getConfiguration();

    $form['linkedin_profile_block_profile_type'] = array(
      '#type' => 'select',
      '#title' => $this->t('Profile type'),
      '#description' => $this->t('Choose member or company profile type'),
      '#options' => [
        'CompanyProfile' => t('Company Profile'),
        'MemberProfile' => t('Member Profile'),
      ],
      '#default_value' => isset($config['profile_type']) ? $config['profile_type'] : '',
    );

    $form['linkedin_profile_block_profile_url'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Public Profile URL'),
      '#description' => $this->t('Enter the URL to your LinkedIn public profile, i. e. <i>https://www.linkedin.com/in/[name]</i> for a member profile<br />or <i>https://www.linkedin.com/company/[company]</i> for a company profile'),
      '#default_value' => isset($config['profile_url']) ? $config['profile_url'] : '',
    );
    $form['linkedin_profile_block_display_mode'] = array(
      '#type' => 'select',
      '#title' => $this->t('Display Mode'),
      '#description' => $this->t('Choose a display mode for the plugin'),
      '#options' => [
        'inline' => t('Inline'),
        'hover' => t('Icon'),
      ],
      '#default_value' => isset($config['display_mode']) ? $config['display_mode'] : '',
    );
    $form['linkedin_profile_block_profile_name'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Profile Name (optional)'),
      '#description' => $this->t('Enter the name to be displayed with the icon if you choose icon as display mode'),
      '#default_value' => isset($config['profile_name']) ? $config['profile_name'] : '',
    );

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->setConfigurationValue('profile_type', $form_state->getValue('linkedin_profile_block_profile_type'));
    $this->setConfigurationValue('profile_url', $form_state->getValue('linkedin_profile_block_profile_url'));
    $this->setConfigurationValue('display_mode', $form_state->getValue('linkedin_profile_block_display_mode'));
    $this->setConfigurationValue('profile_name', $form_state->getValue('linkedin_profile_block_profile_name'));
  }

}
